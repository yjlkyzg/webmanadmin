# webman + pearadmin 后台管理系统

[![Latest Stable Version](http://poser.pugx.org/le/wbadmin/v)](https://packagist.org/packages/le/wbadmin) [![Total Downloads](http://poser.pugx.org/le/wbadmin/downloads)](https://packagist.org/packages/le/wbadmin) [![Latest Unstable Version](http://poser.pugx.org/le/wbadmin/v/unstable)](https://packagist.org/packages/le/wbadmin) [![License](http://poser.pugx.org/le/wbadmin/license)](https://packagist.org/packages/le/wbadmin) [![PHP Version Require](http://poser.pugx.org/le/wbadmin/require/php)](https://packagist.org/packages/le/wbadmin)

#### 介绍
webman + pearadmin 后台管理系统
使用php常驻内存框架 + pearadmin layui + think-orm + laravel-cache + laravel-redis 缓存实现的后台管理系统<br/>
支持阿里云、oss图片上传
已完成后台菜单管理、权限管理、角色管理
<br/>
使用composer 安装 : composer create-project le/wbadmin<br/>

演示地址:
[点击访问演示地址https://wbadmin.itjiale.com](https://wbadmin.itjiale.com/admin)
admin
123456 (请勿修改总管理员密码)<br/>
演示站环境: php8.0 + mysql8.0 + redis

#### laravel-orm 版本后台
[webman-admin(laravel)](https://gitee.com/yjlkyzg/webman-admin)<br/>
主要变化 使用Eloquent-orm,由于后台性能要求不高所以简化后台缓存逻辑,要求php版本更高<br/>

#### 新版LeAdmin后台
[LeAdmin后台](https://gitee.com/yjlkyzg/leadmin)<br/>
主要变化 最低要求php8.0 升级think-orm 3.0, 后端代码更简洁，逻辑更清晰,后续主要更新项目



####  V1.6.5
1. 新增webman-tech/logger日志插件，实现不同模块日志记录到不同目录下
2. 内置app、api、admin、user、queue、model通道，如果需要增加可以在config/plugin里配置
3. 使用示例: Logger::admin("adminlog"); Logger::model(['name'=>'model'],'error');
4. 更多使用方式查看插件地址:https://www.workerman.net/plugin/58
5. 用户登录中间件优化返回Response


####  V1.6.0
1. 新增编译二进制文件，无需环境放到linux即可运行(注：需要php版本大于8.0)
2. 部分优化

二进制编译说明<br/>
./webman build:bin 编译<br/>
生成于Build/webman.bin<br/>
将webman.bin 与.env文件放到linux服务器上，使用./webman start 运行即可<br/>


####  V1.5.0
1. 新增异常handler,统一返回json
2. 统一success、error返回方法
3. constants移至admin模块下
4. 优化前端模板结构布局

####  V1.4.1
新增定时任务组件
修复不强制使用路由时中间件bug
env配置文件增加服务配置

####  V1.4
新增event组件
新增用户模块
前台新增用户注册、登陆接口
后台新增用户列表

####  V1.3
新增phpoffice/phpspreadsheet组件
新增请求记录导出xls(采用后端导出,也可以自己优化为前端导出)


##### V1.2
新增redis-queue队列
新增请求日志记录，由队列记录到mysql

##### V1.1
修改think-cache 为 laravel-cache


#### 软件架构
后端采用webman常驻内存框架
前端采用pearadmin layui版本
使用mysql5.7数据库
使用redis 缓存数据
php 建议使用php7.4 及以后版本



#### 安装教程

1.  使用composer拉取代码 composer create-project le/wbadmin  
2.  导入sql文件(sql目录下)，修改根目录.env文件 配置mysql、redis信息
3.  系统session默认使用redis存储，并且存储时间较长，如果需要修改可以更改 config/session.php 文件

#### 使用说明

1.  根目录 php start.php start 调试模式启动 ， 加上 -d 为生产模式启动
2.  config/server.php 可以配置启动进程数量，建议为服务器核心数量 * 2.

#### 压测结果
服务器使用阿里云 4核8G 峰值50M带宽<br/>
接口统一返回1，不操作数据库，站点开启8进程
ab -n 10000 -c 100 
内网压测结果
![内网压测结果](https://wechat.yangjiale.top/localhostres.png)

外网压测结果(会受带宽、本地网络影响)
![外网压测结果](https://wechat.yangjiale.top/networkres.png)




部分截图
![输入图片说明](https://foruda.gitee.com/images/1667264084231808647/1512e4d3_775530.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1667264109444446081/95623ccb_775530.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1667264119155116119/74aefcb7_775530.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1667264126647705630/65bf4bea_775530.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1667264138856851699/2851bbfd_775530.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1667264154593931539/d4f0febd_775530.png "屏幕截图")
![输入图片说明](https://foruda.gitee.com/images/1667264161859469354/fb2b0778_775530.png "屏幕截图")



#### 免责声明
1. 请您承诺秉着合法、合理的原则使用该后台框架，不利用该框架进行任何违法、侵害他人合法利益等恶意的行为，也不可运用于任何违反我国法律法规的 Web 平台，造成后果将由使用者承担，本团队不承担任何法律责任。
2. 任何单位或个人因下载使用该框架造成的任何意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带或衍生的损失等)，将由使用者承担，本团队不承担任何法律责任。
3. 用户明确并同意本声明条款列举的全部内容，对使用该后台框架可能存在的风险和相关后果将完全由用户自行承担，本团队不承担任何法律责任。

