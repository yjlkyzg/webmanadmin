<?php

namespace app\api\controller;

use app\service\upload\UploadService;
use support\Request;

class UploadController
{

    //单图/多图上传
    public function upload(Request $request){

        $upload = new UploadService();

        try{
            $res = $upload->upload($request->file());
        }catch(\Throwable $throwable){

            return error($throwable->getMessage());
        }

        return success('ok',$res);
    }

}
