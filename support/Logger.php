<?php

namespace support;


/**
 * @method static void admin($msg, string $type = 'info', array $context = [])
 * @method static void api($msg, string $type = 'info', array $context = [])
 * @method static void app($msg, string $type = 'info', array $context = [])
 * @method static void user($msg, string $type = 'info', array $context = [])
 * @method static void queue($msg, string $type = 'info', array $context = [])
 * @method static void model($msg, string $type = 'info', array $context = [])
 */
class Logger extends \WebmanTech\Logger\Logger
{

}